# Xournal++ viewer for Android

This app is capable of opening and displaying .xopp files on Android.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/de.thefeiter.xournalviewer/)

## Current supported features
- Pen strokes
- Marker strokes
- Text
- Geometry (lines, arrows, circles, squares etc.)
- Eraser
- Images
- LaTeX

## ToDo
- Paper background
- PDF background
  - waiting for new bundled .xopp format
- Zoom

## CanDO
- Theming
- Colors
- Read the document title
- Read and display the number of pages
- Page navigation