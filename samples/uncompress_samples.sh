#!/bin/sh

for i in *.xopp;
    do name=`echo "$i" | cut -d'.' -f1`
    ext=`echo "$i" | cut -d'.' -f2`
    echo "$name"
    echo "$ext"
    gzip -d -S .xopp -c "${name}.xopp" > "${name}_uncompressed.xml"
done
