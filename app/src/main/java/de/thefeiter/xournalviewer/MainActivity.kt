package de.thefeiter.xournalviewer

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.pdf.PdfRenderer
import android.net.Uri
import android.os.Bundle
import android.os.ParcelFileDescriptor
import android.util.Base64
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Divider
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextMeasurer
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.drawText
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.rememberTextMeasurer
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.scale
import de.thefeiter.xournalviewer.ui.theme.XournalViewerTheme
import de.thefeiter.xournalviewer.xml.Coordinate
import de.thefeiter.xournalviewer.xml.XoppImage
import de.thefeiter.xournalviewer.xml.XoppPage
import de.thefeiter.xournalviewer.xml.XoppStroke
import de.thefeiter.xournalviewer.xml.XoppTexImage
import de.thefeiter.xournalviewer.xml.XoppText
import de.thefeiter.xournalviewer.xml.XoppVariableStroke
import de.thefeiter.xournalviewer.xml.XoppXmlParser
import org.xmlpull.v1.XmlPullParserException
import java.io.File
import java.io.FileOutputStream
import java.util.zip.GZIPInputStream
import java.util.zip.ZipException


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            XournalViewerTheme {
                // A surface container using the 'background' color from the theme
                val fileChooseResult = remember { mutableStateOf<Uri?>(null) }
                val fileSelectionLauncher =
                    rememberLauncherForActivityResult(ActivityResultContracts.OpenDocument()) {
                        fileChooseResult.value = it
                    }


                this.intent.data?.let { returnUri ->
                    fileChooseResult.value = returnUri
                }


                // use Scaffold to define basic Layout
                Scaffold(
                    topBar = {
                    },
                    floatingActionButton = {
                        OpenFileButton(fileSelectionLauncher)
                    },
                    content = { innerPadding ->
                        val mContext = LocalContext.current
                        Surface(
                            modifier = Modifier
                                .padding(innerPadding),
                        ) {
                            // remember width of lazyColumn
                            val componentWidth = remember { mutableStateOf(0.dp) }
                            // get local density from composable
                            val density = LocalDensity.current

                            LazyColumn(modifier = Modifier
                                .fillMaxSize()
                                .onGloballyPositioned {
                                    // update lazyColumn width
                                    componentWidth.value = with(density) {
                                        it.size.width.toDp()
                                    }
                                }) {


                                fileChooseResult.value?.let {
                                    try {
                                        // load document and read xml
                                        val inputStream = contentResolver.openInputStream(it)
                                        val pages =
                                            XoppXmlParser().parse(GZIPInputStream(inputStream))

                                        pages.forEach { page ->
                                            item {
                                                // calculate needed scale for page canvas size
                                                val scale = componentWidth.value / page.width
                                                // draw page with given size
                                                XoppPageView(
                                                    modifier = Modifier
                                                        .width(scale * page.width)
                                                        .height(scale * page.height), page = page,
                                                    density = density
                                                )
                                            }
                                            // add a divider between pages
                                            item {
                                                Divider()
                                            }
                                        }
                                    } catch (e: ZipException) {
                                        Toast.makeText(
                                            mContext,
                                            "Error opening File. Make sure it is a valid .xopp file. (no gzip)",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    } catch (e: XmlPullParserException) {
                                        Toast.makeText(
                                            mContext,
                                            "Error opening File. Make sure it is a valid .xopp file. (xml error)",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }
                            }
                        }
                    }
                )


            }
        }
    }
}


@Composable
fun XoppPageView(page: XoppPage, modifier: Modifier = Modifier, density: Density) {
    val textMeasurer = rememberTextMeasurer()
    // every page is drawn on a canvas
    Canvas(modifier = modifier) {
        val scale = size.width / page.width  //adjust to screen size

        // draw the background
        drawRect(
            color = Color.White,
            size = Size(page.width, page.height) * scale
        )

        // for each layer draw all elements to the canvas
        page.layers.forEach { layer ->
            layer.elements.forEach { drawable ->
                when (drawable) {
                    is XoppStroke -> drawXoppStroke(this, drawable, scale)
                    is XoppVariableStroke -> drawXoppVariableStroke(this, drawable, scale)
                    is XoppText -> drawXoppText(this, textMeasurer, drawable, scale, density)
                    is XoppImage -> drawXoppImage(this, drawable, scale)
                    is XoppTexImage -> drawXoppTexImage(this, drawable, scale)
                }
            }
        }
    }
}

fun drawXoppImage(drawScope: DrawScope, image: XoppImage, scale: Float) {


    val byteArray = Base64.decode(image.imageData, Base64.DEFAULT)
    var bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)

    bitmap = bitmap.scale(
        ((image.right - image.left) * scale).toInt(),
        ((image.bottom - image.top) * scale).toInt()
    )

    drawScope.drawImage(
        bitmap.asImageBitmap(),
        topLeft = Offset(image.left * scale, image.top * scale),

        )

}

fun drawXoppTexImage(drawScope: DrawScope, image: XoppTexImage, scale: Float) {


    val byteArray = Base64.decode(image.imageData, Base64.DEFAULT)
    val file = File.createTempFile("temp", null).also { FileOutputStream(it).write(byteArray) }
    val pdfRenderer =
        PdfRenderer(ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY))

    var bitmap: Bitmap
    pdfRenderer.openPage(0).use { page ->
        bitmap = Bitmap.createBitmap(
            page.width * 10, // *10 for now, to improve resolution. Not sure why this happens
            page.height * 10,
            Bitmap.Config.ARGB_8888
        ).apply {
            val canvas = android.graphics.Canvas(this)
            canvas.drawBitmap(this, 0f, 0f, null)
        }
        page.render(
            bitmap,
            null,
            null,
            PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY
        )
    }

    bitmap = bitmap.scale(
        ((image.right - image.left) * scale).toInt(),
        ((image.bottom - image.top) * scale).toInt()
    )

    drawScope.drawImage(
        bitmap.asImageBitmap(),
        topLeft = Offset(image.left * scale, image.top * scale),

        )

}

fun drawXoppText(
    drawScope: DrawScope,
    textMeasurer: TextMeasurer,
    xoppText: XoppText,
    scale: Float,
    density: Density
) {
    val textdp = (xoppText.size * scale) / density.density

    var fontFamily = FontFamily.Default

    if (xoppText.font.lowercase().contains("mono|code".toRegex())) {
        fontFamily = FontFamily.Monospace
    } else if (xoppText.font.lowercase().contains("sans")) {
        fontFamily = FontFamily.SansSerif
    } else if (xoppText.font.lowercase().contains("serif")) {
        fontFamily = FontFamily.Serif
    }

    val text = xoppText.text.replace("\t", "\t\t\t\t")


    drawScope.drawText(
        textMeasurer = textMeasurer,
        topLeft = Offset(xoppText.x * scale, xoppText.y * scale),
        text = text,
        style = TextStyle(fontSize = textdp.sp, fontFamily = fontFamily)
    )
}


fun getPathOfXoppStrokeCoordinates(coords: List<Coordinate>, scale: Float): Path {
    val path = Path()
    coords.forEachIndexed { index, coordinate ->

        if (index == 0) {
            path.moveTo(coordinate.x * scale, coordinate.y * scale)
        } else {
            path.lineTo(coordinate.x * scale, coordinate.y * scale)
        }
    }

    return path
}

fun drawXoppStroke(drawScope: DrawScope, stroke: XoppStroke, scale: Float) {
    val path = getPathOfXoppStrokeCoordinates(stroke.path, scale)

    stroke.fill?.let {
        val color = stroke.color.copy(alpha = stroke.color.alpha * 0.5F)
        drawScope.drawPath(
            color = color,
            path = path,
            style = Fill
        )
    }

    drawScope.drawPath(
        color = stroke.color,
        path = path,
        style = Stroke(
            width = stroke.width * scale,
            cap = if (stroke.capStyle == "round") {
                StrokeCap.Round
            } else {
                StrokeCap.Square
            }
        )
    )
}

fun drawXoppVariableStroke(drawScope: DrawScope, stroke: XoppVariableStroke, scale: Float) {
    var start: Coordinate? = null

    stroke.fill?.let {
        val alpha = stroke.color.alpha * (it / 255F)
        val color = stroke.color.copy(alpha = alpha)
        drawScope.drawPath(
            color = color,
            path = getPathOfXoppStrokeCoordinates(stroke.path, scale),
            style = Fill
        )
    }

    stroke.path.forEachIndexed { index, coordinate ->


        start?.let {
            drawScope.drawLine(
                color = stroke.color,
                start = Offset(it.x * scale, it.y * scale),
                end = Offset(coordinate.x * scale, coordinate.y * scale),
                strokeWidth = stroke.widths[index],
                cap = if (stroke.capStyle == "round") {
                    StrokeCap.Round
                } else {
                    StrokeCap.Square
                }
            )
        }

        start = coordinate


    }

}


@Composable
fun OpenFileButton(
    fileSelectionLauncher: ManagedActivityResultLauncher<Array<String>, Uri?>
) {

    FloatingActionButton(
        onClick = {
            fileSelectionLauncher.launch(
                arrayOf(
                    "application/*"
                )
            )
        },
    ) {
        Icon(painter = painterResource(id = R.drawable.ic_open_file), "Floating action button.")
    }
}