package de.thefeiter.xournalviewer.xml

import android.util.Xml
import androidx.compose.ui.graphics.Color
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.zip.GZIPInputStream


// We don't use namespaces.
private val ns: String? = null

class XoppXmlParser {

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(gzInputStream: GZIPInputStream): List<XoppPage> {
        gzInputStream.use {
            val parser: XmlPullParser = Xml.newPullParser()
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            parser.setInput(it, null)
            parser.nextTag()
            return readXoppFile(parser)
        }
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readXoppFile(parser: XmlPullParser): List<XoppPage> {
        val pages = mutableListOf<XoppPage>()

        parser.require(XmlPullParser.START_TAG, ns, "xournal")
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            // Starts by looking for the page tag.
            if (parser.name == "page") {
                pages.add(readPage(parser))
            } else {
                skip(parser)
            }
        }
        return pages
    }


    // Parses the contents of a page.
    @Throws(XmlPullParserException::class, IOException::class)
    private fun readPage(parser: XmlPullParser): XoppPage {
        parser.require(XmlPullParser.START_TAG, ns, "page")

        var width = 595.28F // default A4 sizes in pt
        var height = 841.89F
        if (parser.name == "page") {
            width = parser.getAttributeValue(null, "width").toFloat()
            height = parser.getAttributeValue(null, "height").toFloat()
        }

        val layers = mutableListOf<XoppLayer>()

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            if (parser.name == "layer") {
                layers.add(readLayer(parser))
            } else {
                skip(parser)
            }
        }
        return XoppPage(width, height, layers)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readLayer(parser: XmlPullParser): XoppLayer {
        parser.require(XmlPullParser.START_TAG, ns, "layer")

        val elements = mutableListOf<XoppDrawable>()

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "stroke" -> elements.add(readStroke(parser))
                "text" -> elements.add(readText(parser))
                "image" -> elements.add(readImage(parser))
                "teximage" -> elements.add(readTexImage(parser))
                else -> skip(parser)
            }
        }
        return XoppLayer(elements)
    }


    private fun strToColor(string_in: String): Color {
        val string = string_in.uppercase().replace("#", "")
        return try {

            val r = string.slice(0..1).toInt(16)
            val g = string.slice(2..3).toInt(16)
            val b = string.slice(4..5).toInt(16)
            val a = string.slice(6..7).toInt(16)
            Color(r, g, b, a)
        } catch (_: NumberFormatException) {
            Color.Black
        }


    }

    private fun readStroke(parser: XmlPullParser): XoppStrokeBase {
        parser.require(XmlPullParser.START_TAG, ns, "stroke")
        val tool: String? = parser.getAttributeValue(null, "tool")
        val color: Color = strToColor(parser.getAttributeValue(null, "color"))
        val capStyle: String = parser.getAttributeValue(null, "capStyle")
        val fill: Int? = parser.getAttributeValue(null, "fill")?.toInt()


        val widths = parser.getAttributeValue(null, "width").split(" ")


        val content = readContent(parser)
        val coordinateStrings = content.split(" ")
        assert(coordinateStrings.size % 2 == 0)
        val coordinates = mutableListOf<Coordinate>()
        coordinateStrings.chunked(2).forEach {
            coordinates.add(Coordinate(it[0].toFloat(), it[1].toFloat()))
        }

        parser.require(XmlPullParser.END_TAG, ns, "stroke")

        return if (widths.size == 1) {
            XoppStroke(tool, color, widths[0].toFloat(), capStyle, coordinates, fill)
        } else {
            XoppVariableStroke(
                tool,
                color,
                widths.slice(1..<widths.size).map { it.toFloat() },
                capStyle,
                coordinates,
                fill
            )
        }
    }


    private fun readText(parser: XmlPullParser): XoppText {
        parser.require(XmlPullParser.START_TAG, ns, "text")
        val font: String = parser.getAttributeValue(null, "font")
        val size = parser.getAttributeValue(null, "size").toFloat()
        val x = parser.getAttributeValue(null, "x").toFloat()
        val y = parser.getAttributeValue(null, "y").toFloat()
        val color: Color = strToColor(parser.getAttributeValue(null, "color"))


        val text = readContent(parser)
        parser.require(XmlPullParser.END_TAG, ns, "text")

        return XoppText(font, size, x, y, color, text)
    }


    private fun readImage(parser: XmlPullParser): XoppImage {
        parser.require(XmlPullParser.START_TAG, ns, "image")
        val top = parser.getAttributeValue(null, "top").toFloat()
        val bottom = parser.getAttributeValue(null, "bottom").toFloat()
        val left = parser.getAttributeValue(null, "left").toFloat()
        val right = parser.getAttributeValue(null, "right").toFloat()


        val data = readContent(parser)
        parser.require(XmlPullParser.END_TAG, ns, "image")

        return XoppImage(left, top, right, bottom, data)
    }


    private fun readTexImage(parser: XmlPullParser): XoppTexImage {
        parser.require(XmlPullParser.START_TAG, ns, "teximage")
        val top = parser.getAttributeValue(null, "top").toFloat()
        val bottom = parser.getAttributeValue(null, "bottom").toFloat()
        val left = parser.getAttributeValue(null, "left").toFloat()
        val right = parser.getAttributeValue(null, "right").toFloat()
        val texSource = parser.getAttributeValue(null, "text")


        val data = readContent(parser)
        parser.require(XmlPullParser.END_TAG, ns, "teximage")

        return XoppTexImage(left, top, right, bottom, data, texSource)
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readContent(parser: XmlPullParser): String {
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }


}
