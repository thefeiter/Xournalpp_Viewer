package de.thefeiter.xournalviewer.xml

import androidx.compose.ui.graphics.Color

// management elements
data class XoppPage(val width: Float, val height: Float, val layers: List<XoppLayer>)
data class XoppLayer(val elements: List<XoppDrawable>)

// drawable elements
open class XoppDrawable

open class XoppStrokeBase : XoppDrawable()
data class XoppStroke(
    val tool: String?,
    val color: Color,
    val width: Float,
    val capStyle: String,
    val path: List<Coordinate>,
    val fill: Int? = null
) : XoppStrokeBase()


data class XoppVariableStroke(
    val tool: String?,
    val color: Color,
    val widths: List<Float>,
    val capStyle: String?,
    val path: List<Coordinate>,
    val fill: Int? = null
) : XoppStrokeBase()

data class XoppText(
    val font: String,
    val size: Float,
    val x: Float,
    val y: Float,
    val color: Color,
    val text: String
) : XoppDrawable()


open class XoppImageBase : XoppDrawable()
data class XoppImage(
    val left: Float,
    val top: Float,
    val right: Float,
    val bottom: Float,
    val imageData: String,
) : XoppImageBase()

data class XoppTexImage(
    val left: Float,
    val top: Float,
    val right: Float,
    val bottom: Float,
    val imageData: String,
    val texSource: String,
) : XoppImageBase()


// helper elements
data class Coordinate(val x: Float, val y: Float)